$(function(){
    //get slider height
    
    var WinH   = $(window).height();
    var UpperH = $('.upper-bar').innerHeight();
    var NavH   = $('.navbar').innerHeight();
    $('.slider, .carousel-inner, .overlay, .carousel-item').height(WinH - ( UpperH + NavH )); 
    
    //featured work shuffle images
    
    $('.featured-work ul li').on('click', function(){
        $(this).addClass('active').siblings().removeClass('active');
        
        if($(this).data('class') === '.All'){
            $('.shuffle-images .col-md').css('opacity',1);
        }
        else{
            $('.shuffle-images .col-md').css('opacity','.2');
            $($(this).data('class')).parent().css('opacity',1);
        }
    }); 
});

